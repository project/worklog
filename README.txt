OVERVIEW
--------

This module provides a means to keep track of how much time you spend
on various tasks.  It allows you create timers, one for each task you
work on.  Timers may be paused, resumed, and saved.  When saved a node
is created, recording the total time spent on the task in question.

Once the node is created, it can be edited like any other node.  You
can change the amount of time spent (useful if you forgot to stop a
timer).  Worklog nodes include a bill rate, if you are billing for
hours spent on the project in question.

The module provides two blocks.  One for starting new
timers.  Another for viewing and saving paused timers.  You should
enable both of these blocks if you are using this module.

Contractors can use this module to keep track of billable hours.

HELPFUL HINTS
-------------

Use taxonomy or category modules to categorize your worklog nodes.
For instance, create one vocabulary which is a list of projects.
Another to describe the workflow associated with your time,
i.e. "invoiced", "paid".

Create views (using views module) to see your worklog.

CREDITS
-------

Author: Dave Cohen <http://drupal.org/user/18468>

I stole the name "worklog" from an emacs mode with similar features.

WISHLIST
--------

There are many features I'd like to see but have not had time to
implement.  Including:

* Nice display of worklog.  Show total time and bill.  Show by month
  or two week period.  Basically, create a page that can be used as an
  invoice.

* Improved UI.  I'd like to bring up a small window with just the
  controls to start, pause and stop timers.  (Currently this is done
  through Drupal blocks.)  It could be all AJAXy and slick.  Perhaps
  show the seconds as they tick by.

* Advanced bill calculation.  Configure how hours are rounded,
  i.e. round time spent to 15 minute intervals.

* Restart a timer that has been saved.  (Maybe.  Not sure whether this
  is really necessary.  You can always start a new timer.)

* Better permissions. Control who can view who's worklogs.

Your contributions are welcome.


